<div id="endorsement-<?php print $tid; ?>" class="long">
  <span class="term">
    <span class="counter">
      <?php print $count; ?>
    </span>
    <span class="name">
      <?php print $name; ?>
    </span>
  </span>
  <?php if (!empty($link)): ?>
    <?php print $link; ?>
  <?php endif; ?>
  <hr>
  <span class="endorsers">
    <?php print $pictures; ?>
  </span>
</div>