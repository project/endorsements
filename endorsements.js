(function($) {
    Drupal.behaviors.endorsements = {
        'attach': function(context) {
            $('button.endorse:not(.disabled)', context)
                .bind('click', function() {
                    var btn = $(this);
                    btn.addClass('disabled');
                    $.get('/ajax/user/endorse/' + this.id + '/endorse', null, updateCounter);
                    return false;
                });
        }
    }
    var updateCounter = function(response) {
        $('#endorsement-' + response.tid + ' .counter').html(response.data);
    }
})(jQuery);