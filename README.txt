Drupal endorsements.module README.txt
==============================================================================

This module lets users to endorse any other user in the system. 
The endorsed users can decided whether to accept / reject the endorsements.
[Based on LinkedIn's Endorsement model]

Requirements
------------------------------------------------------------------------------

This module requires Drupal 4.6.

Installation
------------------------------------------------------------------------------

1. Create the SQL table.
     mysql -u username -p password drupal < endorsements.sql

2. Copy endorsements.model under Drupal's modules/endorsements
   directory. Drupal should automatically detect it. Enable the module via
   the administer >> modules page.
   
3. This is an AJAX based implementation, the code should be modified to fit your purpose.
   AJAX functions for accepting / deleting endorsements are endorsements_accept() and endorsements_delete()
   Pending / Accepted endorsements can be fetched by calling endorsements_get() or endorsements_get_pending().
   [Please refer to module for detailed info]

Authors
------------------------------------------------------------------------------

Harshal Dhir <harshal@solutionset.com> [unixguru2k] (active maintainer)
Grady Kuhnline <grady@solutionset.com> (front end ajax part)

History
------------------------------------------------------------------------------

This module idea came is borrowed from LinkedIn's endorsement structure and was used in
internal project of ours and works fairly well and it was originally created by
Harshal Dhir and Grady Kuhnline [AJAX Part]

TODO
------------------------------------------------------------------------------
Well still have to take a look, when i get a good breather.. :-)