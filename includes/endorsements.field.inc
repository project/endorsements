<?php
/**
 * Field hooks for formatting an endorsement taxonomy term reference field.
 */

define('ENDORSEMENT_SHORT_FORMAT', 0);
define('ENDORSEMENT_LONG_FORMAT', 1);

/**
 * Implements hook_field_formatter_info().
 */
function endorsements_field_formatter_info() {
  return array(
    'endorsements_endorsement' => array(
      'label' => t('Endorsement'),
      'field types' => array('taxonomy_term_reference'),
      'settings' => array(
        'format' => ENDORSEMENT_LONG_FORMAT,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function endorsements_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();

  $format = array(
    ENDORSEMENT_SHORT_FORMAT => t('Short format'),
    ENDORSEMENT_LONG_FORMAT => t('Long format'),
  );

  if ($display['type'] == 'endorsements_endorsement') {
    // Add option fields.
    $element['format'] = array(
      '#type'          => 'radios',
      '#title'         => t('Format'),
      '#description'   => t('The short format displays term names and the number of endorsements for that term. The long format additionally displays the user pictures of the last 10 endorsements.'),
      '#default_value' => $settings['format'],
      '#options'       => $format,
    );
  }
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function endorsements_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = '';

  if ($display['type'] == 'endorsements_endorsement') {
    $format = ($settings['format']) ? 'Long format' : 'Short format';
    $summary = t('Format') . ': ' . t('@format', array('@format' => $format)) . '<br>';
  }
  return $summary;
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function endorsements_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  $tids = array();

  // Collect every possible term attached to any of the fieldable entities.
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => $item) {
      // Force the array key to prevent duplicates.
      if ($item['tid'] != 'autocreate') {
        $tids[$item['tid']] = $item['tid'];
      }
    }
  }
  if ($tids) {
    $terms = taxonomy_term_load_multiple($tids);
    // Iterate through the fieldable entities again to attach the loaded term data.
    foreach ($entities as $id => $entity) {
      $rekey = FALSE;
      foreach ($items[$id] as $delta => $item) {
        // Check whether the taxonomy term field instance value could be loaded.
        if (isset($terms[$item['tid']])) {
          // Replace the instance value with the term data.
          $items[$id][$delta]['taxonomy_term'] = $terms[$item['tid']];
          $items[$id][$delta]['endorsers'] = endorsements_has_endorsement($entity, $item['tid']);
        }
        elseif ($item['tid'] == 'autocreate') {
          // Leave the item in place.
        }
        else {
          unset($items[$id][$delta]);
          $rekey = TRUE;
        }
      }
      if ($rekey) {
        // Rekey the items array.
        $items[$id] = array_values($items[$id]);
      }
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function endorsements_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  global $user;

  $element = array();

  // Terms whose tid is 'autocreate' do not exist
  // yet and $item['taxonomy_term'] is not set. Theme such terms as
  // just their name.
  switch ($display['type']) {
    case 'endorsements_endorsement':
      foreach ($items as $delta => $item) {
        if ($item['tid'] == 'autocreate') {
          $element[$delta] = array(
            '#markup' => check_plain($item['name']),
          );
        }
        else {
          $count = count($item['endorsers']);
          $uri = entity_uri('taxonomy_term', $item['taxonomy_term']);
          switch ($display['settings']['format']) {
            case ENDORSEMENT_LONG_FORMAT:
              $format = 'long';
              $pictures = '';
              $endorsers = user_load_multiple($item['endorsers']);
              foreach ($endorsers as $auid => $endorser) {
                $pictures .= theme('endorsement_user_picture', array('account' => $endorser));
              }
              if (($user->uid == $entity->uid) || endorsements_has_endorsed($user->uid, $entity->uid, $item['taxonomy_term']->tid)) {
                // Can't endorse yourself, nor endorse more than once for a given term.
                $link = '';
              }
              else {
                $link = '<button id="endorse-' . $entity->uid . '-' . $item['taxonomy_term']->tid . '" class="endorse" alt="' . t('endorse') . '">+</button>';
              }
              $variables = array(
                'tid' => $item['taxonomy_term']->tid,
                'count' => $count,
                'name' => l($item['taxonomy_term']->name, $uri['path']),
                'link' => $link,
                'pictures' => $pictures,
              );
              break;
            case ENDORSEMENT_SHORT_FORMAT:
              $format = 'short';
              $variables = array(
                'count' => $count,
                'name' => l($item['taxonomy_term']->name, $uri['path']),
              );
              break;
          }
          $element[$delta] = array(
            '#markup' => theme('endorsement_' .$format, $variables),
            '#weight' => -$count,
          );
          drupal_add_js(drupal_get_path('module', 'endorsements') . '/endorsements.js', array('type' => 'file', 'scope' => 'footer'));
          drupal_add_css(drupal_get_path('module', 'endorsements') . '/endorsements.css', 'file');
        }
      }
      uasort($element, 'element_sort');
      break;
  }
  return $element;
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * @see template_preprocess_field().
 */
function endorsements_preprocess_field(&$variables, $hook) {
  $element = $variables['element'];

  if (($element['#bundle'] == 'user') && ($element['#field_name'] == 'field_tags')) {
    $variables['theme_hook_suggestions'][] = 'endorsement_field';
  }
}
