<?php
/**
 * Endorsements pages and forms
 */

function endorsements_list_endorsements($form, &$form_state, $account = NULL) {

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $options = array();
  foreach (module_invoke_all('endorsement_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'accept',
  );
  $options = array();
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $header = array(
    'term' => array('data' => t('Endorsement for')),
    'auid' => array('data' => t('By')),
    'status' => array('data' => t('Status')),
    'created' => array('data' => t('Created')),
    'operations' => array('data' => t('Operations')),
  );

  $endorsements = endorsements_get_endorsements($account);
  if (!empty($endorsements)) {
    // Extract tids and load terms in bulk to save on database calls.
    $endorsements_as_array = json_decode(json_encode($endorsements), TRUE);
    $terms = taxonomy_term_load_multiple(array_column($endorsements_as_array, 'tid'));

    foreach ($endorsements as $eid => $endorsement) {
      $operations = array();
      if ($endorsement->status) {
        $operations[] = l(t('unaccept'), "/user/{$account->uid}/endorsements/{$endorsement->eid}/unaccept");
      }
      else {
        $operations[] = l(t('accept'), "/user/{$account->uid}/endorsements/{$endorsement->eid}/accept");
      }
      if (user_access('manage endorsements')) {
        $operations[] = l(t('delete'), "/user/{$account->uid}/endorsements/{$endorsement->eid}/delete");
      }
      $options[$eid] = array(
        'term' => $terms[$endorsement->tid]->name,
        'auid' => user_load($endorsement->auid)->name,
        'status' => ($endorsement->status) ? t('Accepted') : t('Pending'),
        'created' => format_interval(REQUEST_TIME - $endorsement->created) . ' ' . t('ago'),
        'operations' => implode(' ', $operations),
      );
    }
  }

  $form['endorsements'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No endorsements found.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Submit the endorsements update form.
 */
function endorsements_list_endorsements_submit($form, &$form_state) {
  $operations = module_invoke_all('endorsement_operations', $form, $form_state);
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked endorsements.
  $accounts = array_filter($form_state['values']['endorsements']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($accounts), $operation['callback arguments']);
    }
    else {
      $args = array($accounts);
    }
    call_user_func_array($function, $args);

    drupal_set_message(t('The update has been performed.'));
  }
}

/**
 * Validate the endorsements update form.
 */
function endorsements_list_endorsements_validate($form, &$form_state) {
  $form_state['values']['endorsements'] = array_filter($form_state['values']['endorsements']);
  if (count($form_state['values']['endorsements']) == 0) {
    form_set_error('', t('No endorsements selected.'));
  }
}